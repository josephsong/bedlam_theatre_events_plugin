<?php

	function theatreevents_convert_input_names() {
		while(list($k, $v) = each($_POST)) {
			$baseName = str_replace(THEATREEVENTS_RESERVATION_INPUT_PREFIX, '', $k);
			if (strpos($baseName, THEATREEVENTS_RESERVATION_CUSTOM_INPUT_SUPPLEMENTAL_PREFIX) === 0) {
				$p['customFields'][str_replace(THEATREEVENTS_RESERVATION_CUSTOM_INPUT_SUPPLEMENTAL_PREFIX, '', $baseName)] = $v;
			}
			$p[$baseName] = $v;
		}
		return $p;
	}
	
	function theatreevents_find_input_errors($p = array()) {
		$err = array();
		$securimage = new Securimage();
		
		if (!isset($p['name']) || !$p['name']) {
			$err[THEATREEVENTS_RESERVATION_INPUT_PREFIX . 'name'] = 'Name is required.';
		}
		if (!isset($p['email']) || !$p['email']) {
			$err[THEATREEVENTS_RESERVATION_INPUT_PREFIX . 'email'] = 'Email is required.';
		}
		if (!isset($p['eventdate_id']) || !$p['eventdate_id']) {
			$err[THEATREEVENTS_RESERVATION_INPUT_PREFIX . 'eventdate_id'] = 'You must pick an event date.';
		}
		if (!isset($p['quantity']) || !$p['quantity']) {
			$err[THEATREEVENTS_RESERVATION_INPUT_PREFIX . 'quantity'] = 'You must pick quantity.';
		}	
		if ($securimage->check($_POST['captcha_code']) == false) {
			$err[THEATREEVENTS_RESERVATION_INPUT_PREFIX . 'captcha'] = 'The letters and numbers you entered were incorrect.';
		}
		
		return $err;
	}
	
	function theatreevents_save_the_reservation($p = array()) {
		global $wpdb;
		
		$po = array(
			'name' => $p['name'],
			'phone' => $p['phone'],
			'email' => $p['email'],
			'theatreevent_id' => $p['eventdate_id'],
			'quantity' => $p['quantity'],
			'dinner' => $p['dinner'],
			'comment' => $p['comment'],
			'created' => date('Y-m-d H:i:s')
		);
		
		$inserted = $wpdb->insert(
			$wpdb->prefix . 'theatreevents_reservations', 
			$po, 
			array('%s', '%s', '%s', '%d', '%d', '%s', '%s')
		);
		
		
		if (isset($p['customFields']) && count($p['customFields'])) {
			$insert_id = $wpdb->insert_id;
			while (list($k, $v) = each($p['customFields'])) {
				if ($v) {
					$po = array(
						'theatreevent_setting_id' => $k,
						'theatreevent_reservation_id' => $insert_id,
						'field_value' => $v
					);
					$wpdb->insert(
						$wpdb->prefix . 'theatreevents_reservation_custom_fields', 
						$po, 
						array('%d', '%d', '%s')
					);
				}
			}
		}
	}
	
	
	
	function theatreevents_send_reservation($p = array()) {
		global $wpdb;
		
		$event = $wpdb->get_results(
			'SELECT p.post_title, p.ID, e.* FROM 
			' . $wpdb->prefix . 'posts p, ' . $wpdb->prefix . 'theatreevents e 	
			WHERE e.post_id = p.ID 
			AND e.theatreevent_id = ' . $p['eventdate_id']
		);
		
		$venue = array(
			'name' => 'Bedlam Theatre',
			'email' => 'boxoffice@bedlamtheatre.org',
			'phone' => '612-341-1038',
			'address' => '1501 S. 6th St., Minneapolis, MN 55454',
			'directions_permalink' => 'http://bedlamtheatre.org/directions'
		);
		
		include('templates/email_confirmation.php');
		
		//create a boundary string. It must be unique
		//so we use the MD5 algorithm to generate a random hash
		$random_hash = md5(date('r', time()));
		//define the headers we want passed. Note that they are separated with \r\n
		$headers = "From: boxoffice@bedlamtheatre.org\r\nReply-To: boxoffice@bedlamtheatre.org";
		//add boundary string and mime type specification
		$headers .= "\r\nContent-Type: multipart/alternative; boundary=\"PHP-alt-".$random_hash."\"";
		//define the body of the message.
		ob_start(); //Turn on output buffering
		?>
		<?php echo strip_tags($template); ?> 
		
		<?php
		//copy current buffer contents into $message variable and delete current output buffer
		$message = ob_get_clean();
		//send the email
		@mail( $p['email'], 'Your reservation confirmation from ' . $venue['name'], $message, $headers );
		
	}