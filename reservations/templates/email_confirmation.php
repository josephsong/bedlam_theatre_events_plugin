<?php
	$template = 
"<html>
	<head>
		<title>Your reservation confirmation</title>
	</head>
	<body>
		<h1>Thank you for making a reservation for " . $event[0]->post_title . " at " . $venue['name'] . "</h1>
		<p>We look forward to seeing you there.</p>
		<h2>Here are your reservation details:</h2>
		<ul>
			<li>Your reservation is under the name <strong>" . $p['name'] . "</strong></li>
			<li>Event: <strong><a href=\"" . get_permalink($event[0]->ID) . "\">" . $event[0]->post_title . "</a></strong></li>
			<li>Event date: <strong>" . date('l F, j g:ia', strtotime($event[0]->eventdate)) . "</strong></li>
			<li>Number of tickets: <strong>" . $p['quantity'] . "</strong></li>
		</ul>
		<h3>Here's the information you provided:</h3>
		<ul>
			<li>Name <strong>" . $p['name'] . "</strong></li>
			<li>Email: <strong>" . $p['email'] . "</strong></li>
			<li>Phone: <strong>" . $p['phone'] . "</strong></li>
			<li>Comments: <strong>" . $p['comment'] . "</strong></li>
		</ul>
		<h3>Other useful information:</h3>
		<ul>
			<li>If you need to call us our phone number is <strong>" . $venue['phone'] . "</strong></li>
			<li>If you need <strong><a href=\"" . $venue['directions_permalink'] . "\">directions click here</a></strong></li>
		</ul>
		<p>
			Thanks again. If you need any help with this message 
			<strong><a href=\"mailto:" . $venue['email'] . "\">contact us at " . $venue['email'] . ".</a></strong><br />
			" . $venue['name'] . ", " . $venue['address'] . "
		</p>
	</body>
</html>";