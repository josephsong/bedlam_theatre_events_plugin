<form id="reservation-form" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>form" action="<?php echo WP_PLUGIN_URL; ?>/theatreevents/reservations/submit.php" method="post">
	<h3 class="reservation-form-field-toggle"><a href="#">Make a reservation</a></h3>
	<?php 
		$fieldsClass = array('reservation-form-fields');
		if (!isset($_SESSION['THEATREEVENTS_RESERVATION_SUBMITTED']) || !$_SESSION['THEATREEVENTS_RESERVATION_SUBMITTED']) {
			$fieldsClass[] = 'none';
		} else {
			unset($_SESSION['THEATREEVENTS_RESERVATION_SUBMITTED']);
		}
	?>
	<div class="<?php echo implode(' ', $fieldsClass); ?>">
		<?php theatreevents_print_success_message(); ?> 
		<ul class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>fields">
			<li>
				<label for="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>eventdate_id" class="required">Event Date:</label>
				<select name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>eventdate_id" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>eventdate_id">
					<?php 
						foreach ($post->theatreevents as $event) : 
							$eventSelected = '';
							$eventDisabled = '';
							if (isset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY]['eventdate_id']) && $_SESSION[THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY]['eventdate_id'] == $event->theatreevent_id) :
								$eventSelected = 'selected="selected"';
								unset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY]['eventdate_id']);
							endif;
							if (!$event->allow_reservations || $params['soldOutDates'][$event->theatreevent_id]) :
								$eventDisabled = 'disabled="disabled"';
							endif;
					?>
					<option value="<?php echo $event->theatreevent_id; ?>" <?php echo $eventSelected; ?> <?php echo $eventDisabled; ?>><?php echo date(THEATREEVENTS_DATEFORMAT_SINGLE, strtotime($event->eventdate)); ?></option>
					<?php
						endforeach;
					?>
				</select>
				<?php theatreevents_print_error_for_field('eventdate_id'); ?>
			</li>
			<li>
				<label for="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>quantity" class="required">Number of Tickets:</label>
				<select name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>quantity" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>quantity">
					<?php 
						for ($i = 1; $i <= 15; $i++) :
							$quantitySelected = '';
							if (isset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY]['quantity']) && $_SESSION[THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY]['quantity'] == $i) :
								$quantitySelected = 'selected="selected"';
								unset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY]['quantity']);
							endif;
					?>
					<option value="<?php echo $i; ?>" <?php echo $quantitySelected; ?>><?php echo $i; ?></option>
					<?php 
						endfor; 
					?>
				</select>
				<?php theatreevents_print_error_for_field('quantity'); ?>
			</li>
			
			<?php if ($params['allowDinnerReservations']) { ?>
			<li>
				<label for="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>dinner" class="">Also reserve a table for pre-show dinner:</label>
				<input type="checkbox" name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>dinner" value="1" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>dinner" checked="checked" />
				<?php theatreevents_print_error_for_field('quantity'); ?>
			</li>
			<?php } ?>
			
		</ul>
		<h4>Payment Options</h4>
		
		<?php if ($params['allowPayPal']) { ?>
		
		<fieldset>
			<legend>Pay Now</legend>
			<ul class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>fields">
				<li>
					<!-- PayPal Logo -->
					<input type="submit" name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>submit-paypal" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>paypal" value="Pay via PayPal" />
					<table border="0" cellpadding="10" cellspacing="0" align="center" class="right"><tr><td align="center"></td></tr><tr><td align="center"><a href="#" onclick="javascript:window.open('https://www.paypal.com/us/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="https://www.paypal.com/en_US/i/logo/PayPal_mark_37x23.gif" border="0" alt="Acceptance Mark"></a></td></tr></table><!-- PayPal Logo -->
				</li>
			</ul>	
		</fieldset>
		<h4> -- OR -- </h4>
		
		<?php } ?>
		
		<fieldset>
			<legend>Reserve and Pay at the Door</legend>
			<ul class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>fields">
				<li>
					<label for="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>name" class="required">Your Name:</label>
					<input type="text" value="<?php theatreevents_print_value_for_field('name'); ?>" name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>name" maxlength="255" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>name" />
					<?php theatreevents_print_error_for_field('name'); ?>
				</li>
				<li>
					<label for="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>email" class="required">Email:</label>
					<input type="text" value="<?php theatreevents_print_value_for_field('email'); ?>" name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>email" maxlength="128" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>email" />
					<?php theatreevents_print_error_for_field('email'); ?>
				</li>
				<li>
					<label for="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>phone">Phone:</label>
					<input type="text" value="<?php theatreevents_print_value_for_field('phone'); ?>" name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>phone" maxlength="128" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>phone" />
					<?php theatreevents_print_error_for_field('phone'); ?>
				</li>
				
				
				
				<?php 
					//----- CUSTOM FIELDS ---//
					$customFieldNames = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='custom-field' AND setting_value<>'' AND post_id=" . $post->ID);
					foreach ($customFieldNames as $field) :
				?>
				<li>
					<label for="<?php _e(THEATREEVENTS_RESERVATION_INPUT_PREFIX . THEATREEVENTS_RESERVATION_CUSTOM_INPUT_SUPPLEMENTAL_PREFIX . $field->theatreevent_setting_id); ?>"><?php _e($field->setting_value); ?></label>
					<input type="text" value="<?php theatreevents_print_value_for_field(THEATREEVENTS_RESERVATION_CUSTOM_INPUT_SUPPLEMENTAL_PREFIX . $field->theatreevent_setting_id); ?>" name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX . THEATREEVENTS_RESERVATION_CUSTOM_INPUT_SUPPLEMENTAL_PREFIX . $field->theatreevent_setting_id; ?>" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX . 'custom'; ?>" />
					<?php theatreevents_print_error_for_field(THEATREEVENTS_RESERVATION_CUSTOM_INPUT_SUPPLEMENTAL_PREFIX . $field->theatreevent_setting_id); ?>
				</li>
				<?php
					endforeach;
				?>
				
				
				<li>
					<label for="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>comment">Comment:</label>
					<textarea name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>comment" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>comment"><?php theatreevents_print_value_for_field('comment'); ?></textarea>
					<?php theatreevents_print_error_for_field('comment'); ?>
				</li>
				<li>
					<label for="captcha_code">Enter the letters and numbers you see in the image. Capitalization counts.</label>
					<input type="text" name="captcha_code" size="10" maxlength="6" />
					<img id="captcha" src="<?php echo WP_PLUGIN_URL; ?>/theatreevents/reservations/securimage/securimage_show.php" alt="CAPTCHA Image" />
					<a href="#" onclick="document.getElementById('captcha').src = '<?php echo WP_PLUGIN_URL; ?>/theatreevents/reservations/securimage/securimage_show.php?' + Math.random(); return false">
						<img src="<?php echo WP_PLUGIN_URL; ?>/theatreevents/images/arrow_refresh.png" height="16" width="16" alt="refresh CAPTCHA Image" />
					</a>
					<?php theatreevents_print_error_for_field('captcha'); ?>
				</li>
				<li>
					<input type="submit" name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>submit-willcall" class="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>willcall" value="Reserve and Pay at Door" />
					<input type="hidden" name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>event-id" value="<?php the_ID(); ?>" />
					<input type="hidden" name="<?php echo THEATREEVENTS_RESERVATION_INPUT_PREFIX; ?>referrer" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
				</li>
			</ul>
		</fieldset>
	</div>
</form>
