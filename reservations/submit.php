<?php

	if (!isset($_SESSION)) {
		session_start();
	}
	
	include_once ('../../../../wp-config.php');

	include_once (WP_PLUGIN_DIR . '/theatreevents/utilities/sanitize_input.php');
	include_once (WP_PLUGIN_DIR . '/theatreevents/config.php');
	include_once (WP_PLUGIN_DIR . '/theatreevents/reservations/reservation_functions.php');
	include_once (WP_PLUGIN_DIR . '/theatreevents/reservations/securimage/securimage.php');
	
	if (!isset($_POST) || !count($_POST)) {
		header('Location:/');
		exit();
	}
	
	$_SESSION['THEATREEVENTS_RESERVATION_SUBMITTED'] = true;
	
	$p = theatreevents_convert_input_names();
	$_SESSION[THEATREEVENTS_RESERVATION_SESSION_ERROR_KEY] = theatreevents_find_input_errors($p);
	
	
	
	// defer to alternate reservation systems
	if (isset($p['submit-paypal']) && $p['submit-paypal'] && file_exists(WP_PLUGIN_DIR . '/theatreevents/reservations/third_party/submit_paypal.php')) {
		include(WP_PLUGIN_DIR . '/theatreevents/reservations/third_party/submit_paypal.php');
		exit();
	}
	
	if (!count($_SESSION[THEATREEVENTS_RESERVATION_SESSION_ERROR_KEY])) {
		//save the reservation
		theatreevents_save_the_reservation($p);
		theatreevents_send_reservation($p);
		$_SESSION[THEATREEVENTS_RESERVATION_SESSION_SUCCESS_KEY] = true;
	} else {
		$_SESSION[THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY] = $p;
		$_SESSION[THEATREEVENTS_RESERVATION_SESSION_SUCCESS_KEY] = false;
	}
	
	if (!isset($p['referrer'])) {
		$p['referrer'] = '/';
	}
	
	header('Location:' . $p['referrer'] . '#reservation-form');
	exit();
	
	
	
