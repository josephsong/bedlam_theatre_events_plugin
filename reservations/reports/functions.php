<?php 
function the_eventdate_reservations($eventDateId = 0) {
	global $wpdb, $post;
	$eventDate = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "theatreevents WHERE theatreevent_id=" . $eventDateId);
	if (!$post) {
		$post = get_post($eventDate->post_id);
	}
	$reservations = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "theatreevents_reservations WHERE theatreevent_id=" . $eventDateId);
	$customFields = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "theatreevents_settings WHERE setting_type='custom-field' AND post_id=" . $post->ID);
	
	$caption = 'Reservations for ' . $post->post_title .' @ ' . date('D n/j, g:ia', strtotime($eventDate->eventdate));
?>
	<?php if (!$eventDateId) : ?>
		<p>No event date requested.</p>
	<?php elseif (!$reservations) : ?>
		<p>No reservations for <?php echo date('D n/j, g:ia', strtotime($eventDate->eventdate)); ?> yet.</p>
	<?php else: ?>
	<table class="reservations">
		<caption><?php _e($caption); ?></caption>
		<thead>
			<tr>
				<th>id</th>
				<th>Name</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Qty</th>
				<th>Dinner</th>
				<?php foreach ($customFields as $cf): ?>
				<th><?php _e($cf->setting_value); ?></th>
				<?php endforeach; reset($customFields); ?>
				<th>Comment</th>
				<th>Submitted</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($reservations as $r): ?>
			<tr>
				<th scope="row">
					<?php _e($r->theatreevent_reservation_id); ?>
				</th>
				<td>
					<?php _e($r->name); ?>
				</td>
				<td>
					<?php _e($r->phone); ?>
				</td>
				<td>
					<?php _e($r->email); ?>
				</td>
				<td>
					<?php _e($r->quantity); ?>
				</td>
				<td>
					<?php if ($r->dinner) { ?>
					Y
					<?php } ?>
				</td>
				<?php foreach ($customFields as $cf): ?>
				<td><?php _e($wpdb->get_var("SELECT field_value FROM " . $wpdb->prefix . "theatreevents_reservation_custom_fields WHERE theatreevent_setting_id=" . $cf->theatreevent_setting_id . " AND theatreevent_reservation_id=" . $r->theatreevent_reservation_id)); ?></td>
				<?php endforeach; ?>
				<td>
					<?php _e(str_replace('\r\n', "<br />", $r->comment)); ?>
				</td>
				<td>
					<?php _e($r->created); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
	<?php endif; 
} 
