<?php

	include('../../../../../wp-config.php');
	include('functions.php');
	
	get_currentuserinfo();
    if ($userdata->user_level <= 0) {
    	die('You must be logged in to see this good stuff, it is so GOOD.');
    }
	
	$post = get_post($_GET['post_id']);
	$eventDates = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "theatreevents WHERE post_id=" . $post->ID . " ORDER BY eventdate");
	
?>
<html>
	<head>
		<title><?php _e($caption); ?></title>
		<link type="text/css" rel="stylesheet" href="../../css/baseline.css" />
		<link type="text/css" rel="stylesheet" href="../../css/reports.css" />
	</head>
	<body class="event">
		<?php if (!$_GET['post_id']) : ?>
			<p>No event requested.</p>
		<?php elseif (!$eventDates) : ?>
			<p>This event, <?php _e($post->post_title); ?>, has no event dates attached to it.</p>
		<?php else: ?>
			<?php 
				foreach ($eventDates as $eventDate): 
					the_eventdate_reservations($eventDate->theatreevent_id);
				endforeach;
			?>
		<?php endif; ?>
	</body>
</html>
