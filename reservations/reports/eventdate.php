<?php

	include('../../../../../wp-config.php');
	include('functions.php');
	
	get_currentuserinfo();
    if ($userdata->user_level <= 0) {
    	die('You must be logged in to see this good stuff, it is so GOOD.');
    }
	
	$eventDateId = $_GET['event_id'];
	
	
?>
<html>
	<head>
		<title><?php _e($caption); ?></title>
		<link type="text/css" rel="stylesheet" href="../../css/baseline.css" />
		<link type="text/css" rel="stylesheet" href="../../css/reports.css" />
	</head>
	<body class="eventdate">
		<?php the_eventdate_reservations($eventDateId); ?>
	</body>
</html>
