<?php

	$post = get_post($p['event-id']);
	$eventDate = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "theatreevents WHERE theatreevent_id=" . $p['eventdate_id'] . " AND post_id=" . $p['event-id']);
	
	$postUrl = 'https://www.paypal.com/cgi-bin/webscr';
	//$postUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	
	$paypalData["cmd"] = "_xclick";
	$paypalData["business"] = "boxoffice@bedlamtheatre.org"; //"joe_1234751256_biz@joesong.com";
	$paypalData["undefined_quantity"] = 1;
	$paypalData["quantity"] = $p['quantity'];
	$paypalData["item_name"] = "Bedlam Theatre: " . get_the_title() . " @ " . date('D n/j, g:ia', strtotime($eventDate->eventdate));
	$paypalData["amount"] = number_format(($eventDate->price * 1.029) + 0.3, 2); //2.9% + 30� per transaction
	$paypalData["no_shipping"] = "1";
	$paypalData["cn"] = "Tickets will be held at the box office.";
	$paypalData["return"] = get_permalink() . "?paypalmsg=Your tickets will be held at the box office. Thank you.";
	$paypalData["cancel_return"] = get_permalink() . "?paypalmsg=Sorry you cancelled your order. Please contact us to report a problem.";
	$paypalData["no_note"] = "1";
	$paypalData["currency_code"] = "USD";
	$paypalData["bn"] = "PP-BuyNowBF";
	
	header('Location:' . $postUrl . '?' . http_build_query($paypalData));
