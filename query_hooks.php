<?php 

add_filter('posts_request','theatreevents_posts_request_month_filter');

function theatreevents_posts_request_month_filter($input) {
	/* 
	 * SELECT SQL_CALC_FOUND_ROWS  wp_posts.* 
	 * FROM wp_posts  
	 * WHERE 1=1  
	 * AND YEAR(wp_posts.post_date)='2009' 
	 * AND MONTH(wp_posts.post_date)='8' 
	 * AND wp_posts.post_type = 'post' 
	 * AND (wp_posts.post_status = 'publish')  
	 * ORDER BY wp_posts.post_date DESC LIMIT 0, 4
	 */
	$dateRange = array();
	if (is_month()) {
		$dateRange = get_requested_month_range();
	} else if (is_year()) {
		$dateRange = get_requested_year_range();
	}
	
	if (count($dateRange)) {
		$newInput = theatreevents_get_date_range_query($dateRange['startDate'], $dateRange['endDate']);
		$newInput = str_replace('SELECT', 'SELECT SQL_CALC_FOUND_ROWS', $newInput);
		$input = $newInput;
	}
	return $input;
}
