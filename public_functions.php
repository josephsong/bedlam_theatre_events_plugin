<?php

	add_action('posts_results', 'theatreevents_add_in_events_on_results');
	wp_enqueue_style('theatreevents_widgets', WP_PLUGIN_URL . '/theatreevents/css/widgets.css');
	wp_enqueue_script(
		'theatreevents_widgets_reservation_form', 
		WP_PLUGIN_URL . '/theatreevents/js/widgets/reservation_form.js',
		array('jquery')
	);
					
	
	function theatreevents_get_upcoming_posts($options = array()) {
		$params = array(
			'tag' => THEATRE_EVENTS_FEATURED_TAG_SLUG,
			'limit' => THEATRE_EVENTS_HOME_POST_LIMIT
		);
		if (isset($options['tag'])) {
			$params['tag'] = $options['tag'];
		}
		if (isset($options['showposts'])) {
			$params['limit'] = $options['showposts'];
		}
		
		$events = array();
		$events = array_merge($events, theatreevents_get_tagged_event_posts($params));
		
		if (count($events) < $params['limit'] && (!isset($options['onlyshowtag']) || !$options['onlyshowtag']) ) {
			$params['limit'] -= count($events);
			$params['exclude'] = array();
			foreach ($events as $event) {
				$params['exclude'][] = $event->post_id;
			}
			$events = array_merge($events, theatreevents_get_soonest_event_posts($params));
		}
		
		return theatreevents_get_posts_and_event_from_events($events);
	}
	
	function theatreevents_the_date_range() {
		global $wpdb;
		global $post;
		
		if (count($post->theatreevents) > 1) {
			_theatreevents_theDateRange($post->theatreevents);
		} else {
			_theatreevents_theDate($post->theatreevents);
		}
	}
	
	function theatreevents_get_upcoming_events_by_tag_or_category_name($params = array()) {
		$tag = false;
		$showposts = THEATRE_EVENTS_HOME_POST_LIMIT;
		if (isset($params['tag']) && $params['tag']) {
			$tag = $params['tag'];
		} else if (isset($params['category_name']) && $params['category_name']) {
			$tag = $params['category_name'];
		}
		if (!$tag) { return false; }
		
		return theatreevents_get_upcoming_posts(array('tag' => $tag, 'showposts' => $params['showposts'], 'onlyshowtag' => true));
	}
	
	function theatreevents_add_to_query_string($pairs = array()) {
		$currentQueryStringParts = explode('&', $_SERVER['QUERY_STRING']);
		$currentPairs = array();
		foreach ($currentQueryStringParts as $part) {
			if ($part) {
				$bits = explode('=', $part);
				$currentPairs[$bits[0]] = $bits[1];
			}
		}
		$pairs = array_merge($currentPairs, $pairs);
		$output = array();
		while (list($k, $v) = each($pairs)) {
			$output[] = $k . '=' . $v;
		}
		return implode('&', $output);
	}
	
	function theatreevents_the_event_dates() {
		global $wpdb, $post;
		if (isset($post->theatreevents) && count($post->theatreevents)) : ?>
			<?php if (isset($_GET['paypalmsg'])) : ?>
				<h4 class="success"><?php _e($_GET['paypalmsg']); ?></h4>
			<?php endif; ?>
			<ul class="event-list">
			<?php 
				$reservationsAllowed = false;
				$soldOutDates = array();
				$masterAllowValue = $wpdb->get_var('SELECT setting_value FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='master-allow-reservations' AND post_id=" . $post->ID);
				$masterAllowDinnerValue = $wpdb->get_var('SELECT setting_value FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='master-allow-dinner-reservations' AND post_id=" . $post->ID);
				$masterCutOffTime = $wpdb->get_var('SELECT setting_value FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='master-cutoff-time' AND post_id=" . $post->ID);
				$allowPayPal = false;

				foreach ($post->theatreevents as $event) : 
					if ($masterAllowValue !== '0' && $event->allow_reservations) {
						if (!theatreevents_isSoldOut($event) && !theatreevents_isPastEvent($event, $masterCutOffTime)) {
							$reservationsAllowed = true;
							$soldOutDates[$event->theatreevent_id] = false;
							if ($event->price) {
								$allowPayPal = true;
							}
						} else {
							$soldOutDates[$event->theatreevent_id] = true;
						}
					}
					populateEventDateTemplate($event, $soldOutDates[$event->theatreevent_id]);
				endforeach; ?>
			</ul>
			<?php 
				if ($reservationsAllowed && function_exists('theatreevents_show_reservation_form')) { 
					theatreevents_show_reservation_form(array(
						'soldOutDates' => $soldOutDates, 
						'allowPayPal' => $allowPayPal,
						'allowDinnerReservations' => $masterAllowDinnerValue
					));
				} 
			?>
			<?php endif; 
	}
	
	
