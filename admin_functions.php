<?php

add_action('edit_form_advanced', 'theatreevents_print_master_reservation_fields');
add_action('edit_form_advanced', 'theatreevents_print_date_fields');

add_action('admin_init', 'theatreevents_admin_init');

add_action('save_post', 'theatreevents_save_reservation_settings');
add_action('save_post', 'theatreevents_save_dates');

function theatreevents_print_master_reservation_fields() {
	global $wpdb, $post;
	
	$checkedMasterAllowReservations = 'checked="checked"';
	$checkedMasterAllowDinnerReservations = 'checked="checked"';
	$cutOffTime = 3;
	$numberOfCustomFields = 3;
	$customFieldNames = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='custom-field' AND post_id=" . $post->ID);
	
	$masterAllowValue = $wpdb->get_var('SELECT setting_value FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='master-allow-reservations' AND post_id=" . $post->ID);
	if ($masterAllowValue === '0') {
		$checkedMasterAllowReservations = '';
	}
	
	$masterAllowDinnerValue = $wpdb->get_var('SELECT setting_value FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='master-allow-dinner-reservations' AND post_id=" . $post->ID);
	if ($masterAllowDinnerValue === '0') {
		$checkedMasterAllowDinnerReservations = '';
	}
	
	$cutOffTimeValue = $wpdb->get_var('SELECT setting_value FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='master-cutoff-time' AND post_id=" . $post->ID);
	if ($cutOffTimeValue) {
		$cutOffTime = $cutOffTimeValue;
	}
	
?>
   <div class="postbox" id="postevent-reservation">
		<div title="Click to toggle" class="handlediv"></div>
		<h3 class="hndle"><span>Reservation Settings</span></h3>
		<div class="inside">
			<ul id="theatreevents-admin-reservation-settings">
				<li class="even">
					<label for="theatreevents-master-allow-reservations">
						<input class="theatreevents-master-allow-reservations" name="theatreevents-master-allow-reservations" id="theatreevents-master-allow-reservations" value="1" title="<?php echo THEATREEVENTS_MASTER_ALLOW_RESERVATIONSTIP; ?>" type="checkbox" <?php echo $checkedMasterAllowReservations; ?> />
						Enable reservations (overrides allowing reservations for individual dates)
					</label>
				</li>
				<li class="odd">
					<label for="theatreevents-master-allow-dinner-reservations">
						<input class="theatreevents-master-allow-dinner-reservations" name="theatreevents-master-allow-dinner-reservations" id="theatreevents-master-allow-dinner-reservations" value="1" title="<?php echo THEATREEVENTS_MASTER_ALLOW_DINNER_RESERVATIONSTIP; ?>" type="checkbox" <?php echo $checkedMasterAllowDinnerReservations; ?> />
						Enable dinner reservations for reservable dates
					</label>
				</li>
				<li class="even">
					<input class="theatreevents-master-cutoff-time autoclear" maxlength="8" name="theatreevents-master-cutoff-time" id="theatreevents-master-cutoff-time" value="<?php echo $cutOffTime; ?>" title="<?php echo THEATREEVENTS_CUTOFFTIP; ?>" type="text" />
					<label for="theatreevents-master-cutoff-time">Number of hours before event time to stop allowing reservations</label>
				</li>
<?php
			for ($i = 1; $i <= $numberOfCustomFields; $i++) {
				$customFieldValue = THEATREEVENTS_CUSTOMFIELDTIP;
				$customFieldClass = 'default';
				$customHiddenValue = '';
				if (isset($customFieldNames[$i - 1]) && $customFieldNames[$i - 1]->setting_value) {
					$customFieldValue = $customFieldNames[$i - 1]->setting_value;
					$customHiddenValue = $customFieldNames[$i - 1]->theatreevent_setting_id;
					$customFieldClass = '';
				} 
?>
				<li class="even">
					<input class="theatreevents-master-custom-field-<?php _e($i); ?> autoclear <?php _e($customFieldClass); ?>" maxlength="255" name="theatreevents-master-custom-field-<?php _e($i); ?>" id="theatreevents-master-custom-field-<?php _e($i); ?>" value="<?php echo $customFieldValue; ?>" title="<?php echo THEATREEVENTS_CUSTOMFIELDTIP; ?>" type="text" />
					<input name="theatreevents-master-custom-field-<?php _e($i); ?>-setting-id" value="<?php echo $customHiddenValue; ?>" type="hidden" />
					<label for="theatreevents-master-custom-field-<?php _e($i); ?>">Custom reservation input for this event</label>
				</li>
<?php
			}
?>
				<li>
					<?php 
						$allTotalReservations = theatreevents_getTotalReservationsForEvent($post->ID);
						if ($allTotalReservations) {
							$allTotalReservations = '(' . $allTotalReservations . ')';
						}
					?>
					<a class="theatreevents-see-all-reservations-button button" href="<?php _e(WP_PLUGIN_URL); ?>/theatreevents/reservations/reports/event.php?post_id=<?php _e($post->ID);?>">See All Reservations for All Dates of This Event <?php _e($allTotalReservations); ?></a>
				</li>
			</ul>
			<div>
				
			</div>			
		</div> <!-- end div#postevent-reservation div.inside -->
	</div> <!-- end div#postevent-reservation -->
<?php
}

function theatreevents_print_date_fields()  {
	global $wpdb, $post;
	
	// retreive db rows
	$eventdates = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'theatreevents WHERE post_id=' . $post->ID . ' ORDER BY eventdate');
	$initialFieldsLimit = max(count($eventdates), 0) + 5;
	
?>
   <div class="postbox" id="postevent-dates">
		<div title="Click to toggle" class="handlediv"></div>
		<h3 class="hndle"><span>Event Dates</span></h3>
		<div class="inside">
			<ul id="theatreevents-admin-date-list">
<?php
	
	for ($i = 0; $i < $initialFieldsLimit; $i ++) {
		$liClass = 'mid';
		if ($i == 10) {
			$liClass = 'last';
		}
		require(WP_PLUGIN_DIR . '/theatreevents/input_group_template.php');
	}
			
?>	
			</ul>
			<div>
				<h4>
					<a href="<?php _e(WP_PLUGIN_URL); ?>/theatreevents/services/admin/more_dates.php" id="theatreevents-admin-more-dates-button">+ Add More Dates</a>
				</h4>
			</div>			
		</div> <!-- end div#postevent-dates div.inside -->
	</div> <!-- end div#postevent-dates -->
<?php
}

function theatreevents_admin_init() {
	$filename = basename(preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']));
	if(is_admin() && ($_REQUEST['page'] == 'events-category-options' || $filename == 'post-new.php' || $filename == 'post.php')){
		wp_enqueue_style('admin-theatreevent', WP_PLUGIN_URL . '/theatreevents/css/admin.css');
		wp_enqueue_style('jquery-datepicker', WP_PLUGIN_URL . '/theatreevents/css/ui.all.css');
		wp_enqueue_style('jquery-timepicker', WP_PLUGIN_URL . '/theatreevents/css/jquery.timepickr.css');
		wp_enqueue_style('jquery-timepicker-theme', WP_PLUGIN_URL . '/theatreevents/themes/default/ui.timepickr.css');
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-datepicker', WP_PLUGIN_URL . '/theatreevents/js/ui.datepicker.js');
		wp_enqueue_script('jquery-timepicker-base', WP_PLUGIN_URL . '/theatreevents/js/jquery.timepickr.js');
		wp_enqueue_script('jquery-timepicker', WP_PLUGIN_URL . '/theatreevents/js/ui.timepickr.js');
		wp_enqueue_script('theatreevents-datepicker', WP_PLUGIN_URL . '/theatreevents/js/datepicker.js');
		wp_enqueue_script('theatreevents-autoclear', WP_PLUGIN_URL . '/theatreevents/js/autoclear.js');
		wp_enqueue_script('theatreevents-moredates', WP_PLUGIN_URL . '/theatreevents/js/moredates.js');
	}
}

function theatreevents_save_reservation_settings($post_id, $post=false) {
	global $wpdb;
	
	$post_id = wp_is_post_revision($post_id);
	
	if (!$post_id || $_POST['action'] == 'autosave') {
		return false;
	}
	
	// TODO: move these creation out to plugin activation
	theatreevents_create_theatreevents_settings_table();
	theatreevents_create_reservation_table();
	theatreevents_create_reservation_custom_fields_table();
	
	if (!$_POST['theatreevents-master-allow-reservations']) {
		$_POST['theatreevents-master-allow-reservations'] = 0;
	}
	
	if (!$_POST['theatreevents-master-allow-dinner-reservations']) {
		$_POST['theatreevents-master-allow-dinner-reservations'] = 0;
	}
	
	$masterAllowData = array(
		'setting_type' => 'master-allow-reservations',
		'setting_value' => $_POST['theatreevents-master-allow-reservations'],
		'post_id' => $post_id
	);
	$masterAllowId = $wpdb->get_var('SELECT theatreevent_setting_id FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='" . $masterAllowData['setting_type'] . "' AND post_id=" . $post_id);
	if ($masterAllowId) {
		$wpdb->update($wpdb->prefix . 'theatreevents_settings', $masterAllowData, array('theatreevent_setting_id' => $masterAllowId));
	} else {
		$wpdb->insert($wpdb->prefix . 'theatreevents_settings', $masterAllowData);
	}
	
	$masterAllowDinnerData = array(
		'setting_type' => 'master-allow-dinner-reservations',
		'setting_value' => $_POST['theatreevents-master-allow-dinner-reservations'],
		'post_id' => $post_id
	);
	$masterAllowDinnerId = $wpdb->get_var('SELECT theatreevent_setting_id FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='" . $masterAllowDinnerData['setting_type'] . "' AND post_id=" . $post_id);
	if ($masterAllowDinnerId) {
		$wpdb->update($wpdb->prefix . 'theatreevents_settings', $masterAllowDinnerData, array('theatreevent_setting_id' => $masterAllowDinnerId));
	} else {
		$wpdb->insert($wpdb->prefix . 'theatreevents_settings', $masterAllowDinnerData);
	}
	
	$masterCutoffData = array(
		'setting_type' => 'master-cutoff-time',
		'setting_value' => $_POST['theatreevents-master-cutoff-time'],
		'post_id' => $post_id
	);
	$masterCutoffId = $wpdb->get_var('SELECT theatreevent_setting_id FROM ' . $wpdb->prefix . "theatreevents_settings WHERE setting_type='" . $masterCutoffData['setting_type'] . "' AND post_id=" . $post_id);
	if ($masterCutoffId) {
		$wpdb->update($wpdb->prefix . 'theatreevents_settings', $masterCutoffData, array('theatreevent_setting_id' => $masterCutoffId));
	} else {
		$wpdb->insert($wpdb->prefix . 'theatreevents_settings', $masterCutoffData);
	}
	
	for ($i = 1; isset($_POST['theatreevents-master-custom-field-' . $i . '-setting-id']) && ($_POST['theatreevents-master-custom-field-' . $i . '-setting-id'] || $_POST['theatreevents-master-custom-field-' . $i]) !== THEATREEVENTS_CUSTOMFIELDTIP; $i++) {
		if ($_POST['theatreevents-master-custom-field-' . $i] === THEATREEVENTS_CUSTOMFIELDTIP) {
			$_POST['theatreevents-master-custom-field-' . $i] = '';
		}
		$setting = array(
			'setting_type' => 'custom-field',
			'setting_value' => $_POST['theatreevents-master-custom-field-' . $i],
			'post_id' => $post_id
		);
		if ($_POST['theatreevents-master-custom-field-' . $i . '-setting-id']) {
			$result = $wpdb->update($wpdb->prefix . 'theatreevents_settings', $setting, array('theatreevent_setting_id' => $_POST['theatreevents-master-custom-field-' . $i . '-setting-id']));
		} else {
			if ($_POST['theatreevents-master-custom-field-' . $i]) {
				$wpdb->insert($wpdb->prefix . 'theatreevents_settings', $setting);
			}
		}
	}
}

function theatreevents_save_dates($post_id, $post = false) {
	global $wpdb;
	
	theatreevents_create_theatreevents_table();
	
	$post_id = wp_is_post_revision($post_id);
	
	if (!$post_id) {
		return false;
	}
	
	for($i = 0; $i < THEATREEVENTS_MAXEVENTS; $i++) {
		if (isset($_POST['theatreevents-date-' . $i]) && $_POST['theatreevents-date-' . $i] && $_POST['theatreevents-date-' . $i] !== THEATREEVENTS_DATETIP) {
			//_e($_POST['theatreevents-date-' . $i]);
			$data = array(
				'eventdate' => date('Y-m-d H:i:s', strtotime($_POST['theatreevents-date-' . $i] . ' ' . str_replace(THEATREEVENTS_TIMETIP, '8pm', $_POST['theatreevents-time-' . $i]))),
				'price' => str_replace(THEATREEVENTS_PRICETIP, '', $_POST['theatreevents-price-' . $i]),
				'capacity' => str_replace(THEATREEVENTS_CAPACITYTIP, '', $_POST['theatreevents-capacity-' . $i]),
				'allow_reservations' => $_POST['theatreevents-allow-reservations-' . $i],
				'comment' => str_replace(THEATREEVENTS_COMMENTTIP, '', $_POST['theatreevents-comment-' . $i]),
				'post_id' => $post_id
			);
			
			if ($_POST['theatreevents-id-' . $i]) {
				$wpdb->update(
					$wpdb->prefix . 'theatreevents', 
					$data, 
					array('theatreevent_id' => $_POST['theatreevents-id-' . $i]), 
					array('%s', '%s', '%d', '%s', '%d'),
					array('%d')
				);
			} else {
				$wpdb->insert(
					$wpdb->prefix . 'theatreevents', 
					$data, 
					array('%s', '%s', '%d', '%s', '%d')
				);
			}
		} else if (isset($_POST['theatreevents-date-' . $i]) && $_POST['theatreevents-id-' . $i]) {
			$wpdb->query('DELETE FROM ' . $wpdb->prefix . 'theatreevents WHERE theatreevent_id=' . $_POST['theatreevents-id-' . $i] . ' LIMIT 1');
		}
	}
	
}

function theatreevents_create_theatreevents_table() {
	global $wpdb;
	
	$wpdb->query('CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'theatreevents` (
	  `theatreevent_id` int(11) NOT NULL auto_increment,
	  `eventdate` datetime NOT NULL,
	  `price` varchar(16) default NULL,
	  `capacity` int(11) default NULL,
	  `allow_reservations` tinyint(1) NOT NULL DEFAULT 0,
	  `comment` text,
	  `post_id` int(11) NOT NULL,
	  PRIMARY KEY  (`theatreevent_id`)
	)');
}

function theatreevents_create_theatreevents_settings_table() {
	global $wpdb;
	
	$wpdb->query('CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'theatreevents_settings` (
	  `theatreevent_setting_id` int(11) NOT NULL auto_increment,
	  `setting_type` varchar(128) default NULL,
	  `setting_value` varchar(255) default NULL,
	  `post_id` int(11) NOT NULL,
	  PRIMARY KEY  (`theatreevent_setting_id`)
	)');
}

function theatreevents_create_reservation_table() {
	global $wpdb;
	$wpdb->query('CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'theatreevents_reservations` (
		  `theatreevent_reservation_id` int(11) NOT NULL AUTO_INCREMENT,
		  `name` varchar(255) NOT NULL,
		  `phone` varchar(128) DEFAULT NULL,
		  `email` varchar(128) DEFAULT NULL,
		  `theatreevent_id` int(11) NOT NULL,
		  `quantity` int(11) NOT NULL DEFAULT `1`,
		  `dinner` tinyint DEFAULT `0`,
		  `comment` text,
		  `created` datetime NOT NULL,
		  PRIMARY KEY (`theatreevent_reservation_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;');
}

function theatreevents_create_reservation_custom_fields_table() {
	global $wpdb;
	$wpdb->query('CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'theatreevents_reservation_custom_fields` (
		  `theatreevent_reservation_custom_field_id` int(11) NOT NULL AUTO_INCREMENT,
		  `theatreevent_setting_id` int(11) NOT NULL,
		  `theatreevent_reservation_id` int(11) NOT NULL,
		  `field_value` text,
		  PRIMARY KEY (`theatreevent_reservation_custom_field_id`)
		);');
}