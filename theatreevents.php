<?php
/*
Plugin Name: Theatre Events Calendar
Plugin URI: http://joesong.com
Description: Manages events at a theater.
Version: 0.1
Author: Joe Song
Author URI: http://joesong.com
*/

/*  Copyright 2009  Joe Song  (email : joe@joesong.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define('THEATRE_EVENTS_HOME_POST_LIMIT', 4);
define('THEATRE_EVENTS_FEATURED_TAG_SLUG', 'spotlight');

require(WP_PLUGIN_DIR . '/theatreevents/config.php');

require(WP_PLUGIN_DIR . '/theatreevents/public_functions.php');
require(WP_PLUGIN_DIR . '/theatreevents/private_functions.php');

if (is_admin()) {
	require(WP_PLUGIN_DIR . '/theatreevents/admin_functions.php');
} else {
	require(WP_PLUGIN_DIR . '/theatreevents/query_hooks.php');
	require(WP_PLUGIN_DIR . '/theatreevents/widgets/widgets.php');
}
