<?php

if (isset($_GET['widget'])) {
	switch ($_GET['widget']) {
		case 'calendar':
			include('themes/default/calendar.php');
			break;
	}
}

function theatreevents_service_rewrite($wp_rewrite) {
	$service_rules = array(
		'^/theatreevents/(.+)' =>  'license.txt'
	);
	
	$wp_rewrite->rules = $service_rules + $wp_rewrite->rules;
}

add_filter('generate_rewrite_rules', 'theatreevents_service_rewrite');
