<?php
define('THEATREEVENTS_DATETIP', 'Choose a date...');
define('THEATREEVENTS_TIMETIP', 'Choose a time...');
define('THEATREEVENTS_PRICETIP', 'Price');
define('THEATREEVENTS_CAPACITYTIP', 'Limit');
define('THEATREEVENTS_MASTER_ALLOW_RESERVATIONSTIP', 'Toggle reservations for this event');
define('THEATREEVENTS_MASTER_ALLOW_DINNER_RESERVATIONSTIP', 'Toggle dinner reservations for this event');
define('THEATREEVENTS_ALLOW_RESERVATIONSTIP', 'Toggle reservations for this date');
define('THEATREEVENTS_COMMENTTIP', 'Enter a comment...');
define('THEATREEVENTS_CUTOFFTIP', 'Hours');
define('THEATREEVENTS_CUSTOMFIELDTIP', 'Enter a custom reservation field...');

define('THEATREEVENTS_MAXEVENTS', 50);
define('THEATREEVENTS_DATEFORMAT_SINGLE', 'D, M j, Y - g:ia');
define('THEATREEVENTS_DATEFORMAT_RANGE', 'D, M j, Y');
define('THEATREEVENTS_DATEFORMAT_RANGE_IF_SAME_YEAR', 'D, M j');

define('THEATREEVENTS_RESERVATION_SUBMITTED', 'THEATREEVENTS_RESERVATION_SUBMITTED');
define('THEATREEVENTS_RESERVATION_INPUT_PREFIX', 'theatreevents-reservation-');
define('THEATREEVENTS_RESERVATION_CUSTOM_INPUT_SUPPLEMENTAL_PREFIX', 'custom-');
define('THEATREEVENTS_RESERVATION_SESSION_ERROR_KEY', 'theatreevents-reservation-errors');
define('THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY', 'theatreevents-reservation-values');
define('THEATREEVENTS_RESERVATION_SESSION_SUCCESS_KEY', 'theatreevents-reservation-success');

$eventdates = array();
