<?php
/*
 * These are functions not meant to be used directly by the templates
 */
 
 function theatreevents_get_events_for_post($post) {
 	if (!is_admin()) {
	 	global $wpdb;
		$query = 	"SELECT * " .
					"FROM $wpdb->prefix" . "theatreevents " . 
					"WHERE post_id=" . $post->ID . ' ' .
					"ORDER BY eventdate";
		$results = $wpdb->get_results($query);
		return $results;
	}
}



function theatreevents_get_events_by_date_range($startDate, $endDate) {
	global $wpdb;
	$query = theatreevents_get_date_range_query($startDate, $endDate);
	$results = $wpdb->get_results($query);
	
	return $results;
}

function theatreevents_get_date_range_query($startDate, $endDate) {
	global $wpdb;
	$query = 		"SELECT te.*, " . $wpdb->prefix . "posts.* " .
					"FROM " .
						$wpdb->prefix . "theatreevents te, " .
						$wpdb->prefix . "posts " . $wpdb->prefix . "posts " .
					"WHERE te.post_id = " . $wpdb->prefix . "posts.ID " .
					"AND " . $wpdb->prefix . "posts.post_status = 'publish' " . 
					"AND " . $wpdb->prefix . "posts.post_type = 'post' " .
					"AND te.eventdate >= '$startDate' ".
					"AND te.eventdate < '$endDate' ";
	return $query;
}

function get_requested_week_range() {
	global $wp_query;
	if (isset($_GET['teWeekStart'])) {
		$startTime = strtotime($_GET['teWeekStart']);
	} else {
		$startTime = time();
	}
	return array(
		'startDate' => date('Y-m-d 00:00:00', $startTime), 
		'endDate' => date('Y-m-d 23:59:59', $startTime + 518400)
	);
}

function get_requested_month_range() {
	global $wp_query;
	if ($wp_query->query_vars['year']) {
		$startTime = strtotime($wp_query->query_vars['year'] . '-' . $wp_query->query_vars['monthnum'] . '-1');
	} else {
		$startTime = strtotime(date('Y-m-01'));
	}
	return array(
		'startDate' => date('Y-m-01 00:00:00', $startTime), 
		'endDate' => date('Y-m-t 23:59:59', $startTime)
	);
}

function get_requested_year_range() {
	global $wp_query;
	if ($wp_query->query_vars['year']) {
		$startTime = strtotime($wp_query->query_vars['year'] . '-01-01');
	} else {
		$startTime = strtotime(date('Y-01-01'));
	}
	return array(
		'startDate' => date('Y-01-01 00:00:00', $startTime), 
		'endDate' => date('Y-12-31 23:59:59', $startTime)
	);
}

function theatreevents_get_tagged_event_posts($params) {
	global $wpdb;
	$today = date('Y-m-d 00:00:00');
	$query = 		"SELECT te.* " .
					"FROM " .
						"$wpdb->prefix" . "theatreevents te, " .
						"$wpdb->prefix" . "posts p, " .  
						"$wpdb->prefix" . "term_relationships tr, " . 
						"$wpdb->prefix" . "term_taxonomy tt, " . 
						"$wpdb->prefix" . "terms t " .   
					"WHERE te.post_id = p.ID " .
					"AND p.post_status = 'publish' " . 
					"AND p.ID = tr.object_id " . 
					"AND tr.term_taxonomy_id = tt.term_taxonomy_id " .  
					"AND tt.term_id = t.term_id " .  
					"AND ( " . 
						"t.slug = '" . $params['tag'] . "' " .
						"OR t.name = '" . $params['tag'] . "' " .
					") " . 
					"AND te.eventdate > '$today' ";
	
	if (isset($params['exclude']) && count($params['exclude'])) {
		$query .= 	"AND te.post_id NOT IN (" . implode(',', $params['exclude']) . ") ";
	}
	
	$query .= 		"GROUP BY te.post_id " . 
					"ORDER BY te.eventdate ";
					
	if (isset($params['limit'])) {
		$query .=	"LIMIT " . $params['limit'] . ' ';
	}
	$results = $wpdb->get_results($query);
	
	return $results;
	
}

function theatreevents_get_soonest_event_posts($params = array()) {
	global $wpdb;
	$today = date('Y-m-d 00:00:00');
	$query = 		"SELECT te.* " .
					"FROM $wpdb->prefix" . "theatreevents te,  $wpdb->prefix" . "posts p " .  
					"WHERE te.post_id = p.ID " .
					"AND p.post_status = 'publish' " . 
					"AND te.eventdate > '$today' ";
					
	if (isset($params['exclude']) && count($params['exclude'])) {
		$query .= 	"AND te.post_id NOT IN (" . implode(',', $params['exclude']) . ") ";
	}
	
	$query .= 		"GROUP BY te.post_id " . 
					"ORDER BY te.eventdate ";
					
	if (isset($params['limit'])) {
		$query .=	"LIMIT " . $params['limit'] . ' ';
	}
	
	return $results = $wpdb->get_results($query);
}

function theatreevents_get_posts_and_event_from_events($events = array()) {
	$posts = array();
	foreach ($events as $event) {
		$tempPost = get_post($event->post_id);
		$posts[] = theatreevents_add_in_events_on_results($tempPost);
	}
	
	return $posts;
}
 
function theatreevents_add_in_events_on_results($postData) {
		
	if (!is_array($postData)) {
		$postData->theatreevents = theatreevents_get_events_for_post($postData);
	} else {
		foreach($postData as &$post) {
			$post->theatreevents = theatreevents_get_events_for_post($post);
		}
	}
	return $postData;
}
	
function _theatreevents_theDate($dates) {
	global $post;
	if ($dates[0]) {
		$firstDate = $dates[0];
		echo date(THEATREEVENTS_DATEFORMAT_SINGLE, strtotime($firstDate->eventdate));
	}
}

function _theatreevents_theDateRange($dates) {
	$firstDate = $dates[0];
	$firstDate = strtotime($firstDate->eventdate);
	$lastDate = end($dates);
	$lastDate = strtotime($lastDate->eventdate);
	echo _theatreevents_get_formatted_date_range_by_timestamps($firstDate, $lastDate);
}

function _theatreevents_get_formatted_date_range_by_timestamps($firstDate, $lastDate) {
	$output = array();
	if (date('Y', $firstDate) == date('Y', $lastDate)) {
		$output[] = date(THEATREEVENTS_DATEFORMAT_RANGE_IF_SAME_YEAR, $firstDate);
	} else {
		$output[] = date(THEATREEVENTS_DATEFORMAT_RANGE, $firstDate);
	}
	$output[] = ' - ';
	$output[] = date(THEATREEVENTS_DATEFORMAT_RANGE, $lastDate);
	return implode('', $output);
}

function theatreevents_getTotalReservationsForEvent($post_id) {
	global $wpdb;
	$results = $wpdb->get_results(
		'SELECT SUM(tr.quantity) as total FROM ' . 
		$wpdb->prefix . 'theatreevents_reservations tr, ' .
		$wpdb->prefix . 'theatreevents te
		WHERE tr.theatreevent_id = te.theatreevent_id 
		AND te.post_id=' . $post_id
	);
	return $results[0]->total;
}

function theatreevents_isSoldOut($event) {
	if (!isset($event->capacity) || !$event->capacity) {
		return false;
	}
	$total = theatreevents_getTotalReservationsForEventDate($event);
	if ($total < $event->capacity) {
		return false;
	}
	return true;
}

function theatreevents_getTotalReservationsForEventDate($event) {
	global $wpdb;
	$results = $wpdb->get_results(
		'SELECT SUM(quantity) as total FROM ' . $wpdb->prefix . 'theatreevents_reservations
		WHERE theatreevent_id = ' . $event->theatreevent_id
	);
	return $results[0]->total;
}

function theatreevents_isPastEvent($event, $masterCutOffTime = 0) {
	if (!$masterCutOffTime) {
		$masterCutOffTime = 0;
	}
	$expirationTime = strtotime($event->eventdate . ' -' . $masterCutOffTime . ' hours');
	if (time() < $expirationTime) {
		return false;
	}
	return true;
}

function populateEventDateTemplate($event, $soldOut = false) {
?>
	<li class="clearfloats">
		<div class="event-date">
			<?php echo date('D n/j, g:ia', strtotime($event->eventdate)); ?>
		</div>
		<?php if ($event->price) : ?>
		<div class="event-price">
			<?php echo '$' . $event->price; ?>
		</div>
		<?php endif; ?>
		<?php if ($event->comment && $event->comment != '0'): ?>
		<div class="event-comment">
			<?php echo $event->comment; ?>
		</div>
		<?php endif; ?>
		<div class="event-sold-out right smalltext">
			<?php if ($soldOut): ?>
			<strong>Online reservations closed.</strong>
			<?php endif; ?>
		</div>
	</li>
<?php
}

function theatreevents_print_error_for_field($field) {
	if (isset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_ERROR_KEY][THEATREEVENTS_RESERVATION_INPUT_PREFIX . $field])) { 
		echo '<div class="invalid">' . $_SESSION[THEATREEVENTS_RESERVATION_SESSION_ERROR_KEY][THEATREEVENTS_RESERVATION_INPUT_PREFIX . $field] . '</div>';
		unset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_ERROR_KEY][THEATREEVENTS_RESERVATION_INPUT_PREFIX . $field]); 
	}
}

function theatreevents_print_value_for_field($field) {
	if (isset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY][$field])) {
		echo str_replace('\r\n', "\n", $_SESSION[THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY][$field]);
		unset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_VALUES_KEY][$field]);
	}
}

function theatreevents_print_success_message() {
	if (isset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_SUCCESS_KEY]) && $_SESSION[THEATREEVENTS_RESERVATION_SESSION_SUCCESS_KEY]) {
		echo '<h4 class="success">Your reservation has been made. You will receive an email confirmation. Thank you!</h4>';
	} else if (isset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_SUCCESS_KEY])) {
		echo '<h4 class="invalid">We had a problem processing your reservation. Please correct the errors below and try again.</h4>';
	}
	unset($_SESSION[THEATREEVENTS_RESERVATION_SESSION_SUCCESS_KEY]);
}
