<?php
	$startTime = strtotime($dateRange['startDate']);
	$endTime = strtotime($dateRange['endDate']);
	$dateRangeString = _theatreevents_get_formatted_date_range_by_timestamps($startTime, $endTime);
	
	$previousTimestamp = strtotime($dateRange['startDate'] . ' -1 week');
	$previousUrl = '?' . theatreevents_add_to_query_string(array('teWeekStart' => date('Y-m-d', $previousTimestamp)));
	$previousAjaxUrl = WP_PLUGIN_URL . '/theatreevents/services/calendar/week.php' . $previousUrl;
	$previousText = '&laquo; ' . date('M d, y', $previousTimestamp);
	$nextTimestamp = strtotime($dateRange['startDate'] . ' +1 week');
	$nextUrl = '?' . theatreevents_add_to_query_string(array('teWeekStart' => date('Y-m-d', $nextTimestamp)));
	$nextAjaxUrl = WP_PLUGIN_URL . '/theatreevents/services/calendar/week.php' . $nextUrl;
	$nextText = date('M d, y', $nextTimestamp) . ' &raquo;';
	
	$monthUrl = get_month_link( date('Y'), date('m'));
	$monthIcon = WP_PLUGIN_URL . '/theatreevents/images/calendar.png';
?>

<div class="te-calendar te-widget-week">
	
	<h4 class="te-head">Coming Up</h4>
	<h5 class="te-allmonth"><a href="<?php echo $monthUrl; ?>" class="right" title="See this month's calendar">
		(see all events this month)
	</a></h5>
	<div class="clearabove"></div>
	<h6 class="te-date-range"><?php echo $dateRangeString; ?></h6>
	
	<div class="navigation clearfloats">
		<div class="left"><a href="<?php echo $previousUrl; ?>" class="te-calendar-nav" rel="<?php echo $previousAjaxUrl; ?>"><?php echo $previousText; ?></a></div>
		<div class="right"><a href="<?php echo $nextUrl; ?>" class="te-calendar-nav" rel="<?php echo $nextAjaxUrl; ?>"><?php echo $nextText; ?></a></div>
	</div>
	
	<ul class="te-week-list">
	<?php
		for ($runningDate = date('Y-m-d', strtotime($dateRange['startDate'])); $runningDate <= $dateRange['endDate']; $runningDate = date('Y-m-d', strtotime($runningDate . ' +1 day'))) {
	?>
		<li>
			<div class="te-date"><?php echo date('l, M. j', strtotime($runningDate)); ?></div>
						
	<?php	if (!isset($resultsByDate[$runningDate]) || count($resultsByDate[$runningDate]) == 0) { ?><ul><li class="inactive">&nbsp</li></ul><?php continue; } ?>
						
			<ul>
	<?php	foreach ($resultsByDate[$runningDate] as $post) { ?>
				<li>
					<div class="te-title">
						<a href="<?php the_permalink(); ?>">
							<span class="te-time"><?php echo date('g:ia', strtotime($post->relevantEventDate)); ?></span>
							<?php the_title(); ?>
						</a>
					</div>
				</li>
	<?php	} ?>
			</ul>
		</li>
	<?php	} ?>
	</ul>
	
	<div class="navigation clearfloats">
		<div class="left"><a href="<?php echo $previousUrl; ?>" class="te-calendar-nav" rel="<?php echo $previousAjaxUrl; ?>"><?php echo $previousText; ?></a></div>
		<div class="right"><a href="<?php echo $nextUrl; ?>" class="te-calendar-nav" rel="<?php echo $nextAjaxUrl; ?>"><?php echo $nextText; ?></a></div>
	</div>
</div>