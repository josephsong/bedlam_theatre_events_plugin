<?php
	$activePeriodStart = $dateRange['startDate'];
	$activePeriodEnd = $dateRange['endDate'];
	
	$previousTimestamp = strtotime($dateRange['startDate'] . ' -1 year');
	$previousUrl = get_year_link( date('Y', $previousTimestamp));
	$previousText = '&laquo; ' . date('Y', $previousTimestamp);
	$nextTimestamp = strtotime($dateRange['startDate'] . ' +1 year');
	$nextUrl = get_year_link( date('Y', $nextTimestamp));
	$nextText = date('Y', $nextTimestamp) . ' &raquo;';
	
	$i = 0;
	if (date('w', strtotime($dateRange['startDate'])) !== 0) {
		$dateRange['startDate'] = date('Y-m-d', strtotime($dateRange['startDate'] . ' - 1 Sunday'));
	}
	if (date('w', strtotime($dateRange['endDate'])) !== 6) {
		$dateRange['endDate'] = date('Y-m-d', strtotime($dateRange['endDate'] . ' + 1 Saturday'));
	}
	
	
?>

<div class="te-calendar te-widget-year">
	<div class="navigation clearfloats">
		<div class="left"><a href="<?php echo $previousUrl; ?>"><?php echo $previousText; ?></a></div>
		<div class="right"><a href="<?php echo $nextUrl; ?>"><?php echo $nextText; ?></a></div>
	</div>
	
	<table class="te-month-table">
		<tr>
			<th>Sunday</th>
			<th>Monday</th>
			<th>Tuesday</th>
			<th>Wednesday</th>
			<th>Thursday</th>
			<th>Friday</th>
			<th>Saturday</th>
		</tr>
		<?php for ($runningDate = $dateRange['startDate']; $runningDate < $dateRange['endDate']; $runningDate = date('Y-m-d', strtotime($runningDate . ' + 1 day'))) { ?>
		
			<?php if (!($i % 7)) { ?>
			
		<tr>
			<?php } ?>
			
			<?php
				$tdClass = 'active';
				$tdCaption = false;
				if (($runningDate < $activePeriodStart && $runningDate . ' 00:00:00' != $activePeriodStart) || $runningDate > $activePeriodEnd) {
					// if not in the current month
					$tdClass = 'hidden';
				}else if (!isset($resultsByDate[$runningDate])) { 
					$tdClass = 'inactive'; 
					$tdCaption = 'No events';
				} 
			?>
			
			<td class="<?php echo $tdClass; ?>">
			<h5 class="te-date"><?php echo date('M j', strtotime($runningDate)); ?></h5>
			<?php if ($tdCaption) { ?>
				<div class="te-caption"><?php echo $tdCaption; ?></div>
			<?php } ?>
			
			<?php if (isset($resultsByDate[$runningDate])) { ?>
				<ul>
				<?php foreach ($resultsByDate[$runningDate] as $post) { ?>
					<li>
						<div class="te-title">
							<a href="<?php the_permalink(); ?>">
								<span class="te-time"><?php echo date('g:ia', strtotime($post->relevantEventDate)); ?></span>
								<?php the_title(); ?>
							</a>
						</div>
					</li>
				<?php } ?>
				</ul>
			<?php } ?>
			
			<?php $i++; ?>
			<?php if (!($i % 7)) { ?>
			
		</tr>
			<?php } ?>
		<?php } ?>
	
	</table>
	
	<div class="navigation clearfloats">
		<div class="left"><a href="<?php echo $previousUrl; ?>"><?php echo $previousText; ?></a></div>
		<div class="right"><a href="<?php echo $nextUrl; ?>"><?php echo $nextText; ?></a></div>
	</div>
</div>