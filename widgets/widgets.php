<?php

	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-blockui', WP_PLUGIN_URL . '/theatreevents/js/jquery.blockUI.js');
	wp_enqueue_script('theatreevents-widgets-ajax', WP_PLUGIN_URL . '/theatreevents/js/widgets/ajax.js');

	function theatreevents_show_calendar($params = array('period' => 'week')) {
		global $wpdb, $post, $posts, $wp_query;
		$startDate = date('Y-m-d 00:00:00');
		$endDate = date('Y-m-d 23:59:59');
		$resultsByDate = array();
		
		switch ($params['period']) {
			case 'year':
				$dateRange = get_requested_year_range();
				break;
			case 'month':
				$dateRange = get_requested_month_range();
				break;
			default: // week
				$params['period'] = 'week';
				$dateRange = get_requested_week_range();
				break;
		}
		
		if (!is_month() || $params['period'] !== 'month') { // if is_month() posts are retrieved in main query. Ref: query_hooks.php
			$posts = theatreevents_get_events_by_date_range($dateRange['startDate'], $dateRange['endDate']);
		} 
		
		foreach ($posts as $post) {
			$fullPost = get_post($post->post_id);
			$fullPost->relevantEventDate = $post->eventdate;
			$resultsByDate[date('Y-m-d', strtotime($post->eventdate))][] = $fullPost;
		}
		
		include('templates/' . $params['period'] . '.php');
		
	}
	
	function theatreevents_show_reservation_form($params = array()) {
		global $wpdb, $post;
		include(WP_PLUGIN_DIR . '/theatreevents/reservations/templates/reservation_form.php');
	}
