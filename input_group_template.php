<?php 
	
	$dateValue = THEATREEVENTS_DATETIP;
	$timeValue = THEATREEVENTS_TIMETIP;
	$priceValue = THEATREEVENTS_PRICETIP;
	$capacityValue = THEATREEVENTS_CAPACITYTIP;
	$commentValue = THEATREEVENTS_COMMENTTIP;
	$checkedAllowReservations = 'checked="checked"';
	$idValue = '';
	unset($currentTotal);
	
	$dateClass = $timeClass = $priceClass = $capacityClass = $commentClass = 'default';
	
	if (isset($eventdates[$i]) && $eventdates[$i]) {
		$dateValue = date('m/d/Y', strtotime($eventdates[$i]->eventdate));
		$dateClass = '';
		$timeValue = date('g:ia', strtotime($eventdates[$i]->eventdate));
		$timeClass = '';
		$idValue = $eventdates[$i]->theatreevent_id;
		if ($eventdates[$i]->price) {
			$priceValue = $eventdates[$i]->price;
			$priceClass = '';
		}
		if ($eventdates[$i]->capacity) {
			$capacityValue = $eventdates[$i]->capacity;
			$capacityClass = '';
		}
		if ($eventdates[$i]->allow_reservations == 0) {
			$allowReservationsClass = '';
			$checkedAllowReservations = '';
		}
		if ($eventdates[$i]->comment) {
			$commentValue = $eventdates[$i]->comment;
			$commentClass = '';
		}
		$currentTotal = theatreevents_getTotalReservationsForEventDate($eventdates[$i]);
		if ($currentTotal) {
			$currentTotal = '(' . $currentTotal . ')';
		}
	}
?>
<?php if ($i % 2) { $liClass .= ' odd'; } else { $liClass .= ' even'; } ?>
<li class="<?php echo $liClass; ?> date-row">
	<h5>Date <?php echo $i + 1; ?></h5>
	<ul class="theatreevents-group">
	    <li>
	        <input class="theatreevents-date autoclear <?php _e($dateClass); ?>" name="theatreevents-date-<?php echo $i; ?>" id="theatreevents-date-<?php echo $i; ?>" value="<?php echo $dateValue; ?>" title="<?php echo THEATREEVENTS_DATETIP; ?>" type="text" />
		</li>
		<li>
			<input class="theatreevents-time autoclear <?php _e($timeClass); ?>" name="theatreevents-time-<?php echo $i; ?>" id="theatreevents-time-<?php echo $i; ?>" value="<?php echo $timeValue; ?>" title="<?php echo THEATREEVENTS_TIMETIP; ?>" type="text" />
	    </li>
	    <li>
	        $<input class="theatreevents-price autoclear <?php _e($priceClass); ?>" maxlength="8" name="theatreevents-price-<?php echo $i; ?>" id="theatreevents-price-<?php echo $i; ?>" value="<?php echo $priceValue; ?>" title="<?php echo THEATREEVENTS_PRICETIP; ?>" type="text" />
	    </li>
	</ul>
	<ul class="theatreevents-group">
		<li>
			<label>
				<input class="theatreevents-allow-reservations <?php _e($allowReservationsClass); ?>" name="theatreevents-allow-reservations-<?php echo $i; ?>" id="theatreevents-allow-reservations-<?php echo $i; ?>" value="1" title="<?php echo THEATREEVENTS_ALLOW_RESERVATIONSTIP; ?>" type="checkbox" <?php echo $checkedAllowReservations; ?> />
				Allow Reservations
	        </label>
	    </li>
	    <li>
	        Capacity<input class="theatreevents-capacity autoclear <?php _e($capacityClass); ?>" name="theatreevents-capacity-<?php echo $i; ?>" id="theatreevents-capacity-<?php echo $i; ?>" value="<?php echo $capacityValue; ?>" title="<?php echo THEATREEVENTS_CAPACITYTIP; ?>" type="text" />
	    </li>
		<li>
			<a class="theatreevents-see-reservations-buttons button" href="<?php _e(WP_PLUGIN_URL); ?>/theatreevents/reservations/reports/eventdate.php?event_id=<?php _e($idValue);?>">See Reservations <?php _e($currentTotal); ?></a>
		</li>
	</ul>
	<ul class="theatreevents-group">
		<li>
	        <textarea class="theatreevents-comment autoclear <?php _e($commentClass); ?>" name="theatreevents-comment-<?php echo $i; ?>" id="theatreevents-comment-<?php echo $i; ?>" title="<?php echo THEATREEVENTS_COMMENTTIP; ?>" type="text" ><?php echo $commentValue; ?></textarea>
	    </li>
	</ul>
	<input type="hidden" name="theatreevents-id-<?php echo $i; ?>" value="<?php echo $idValue; ?>" />
</li>
