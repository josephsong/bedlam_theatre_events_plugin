jQuery(document).ready(function() {
	try {
		
		var moreDates = 5;
		var currentDateLimit = 0;
		
		function handleMoreDates(e) {
			var trgt = jQuery(e.target);
			e.preventDefault();
			var currentDateLimit = parseInt(jQuery(jQuery(jQuery('#theatreevents-admin-date-list li.date-row:last')[0]).find('h5')[0]).html().replace('Date ', ''));

			jQuery.get(
				e.target.href, 
				{
					moreDates: moreDates,
					currentDateLimit: currentDateLimit
				}, 
				function(html) {
					jQuery('#theatreevents-admin-date-list').append(html);
				}, 
				'html');
		}

		jQuery('#theatreevents-admin-more-dates-button').bind('click', handleMoreDates);

	} catch(err) {
		console.dir(err);
	}
});

