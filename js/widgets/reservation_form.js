jQuery(document).ready(function(){
	function showFields(evt) {
		evt.preventDefault();
		jQuery(this).parent('form').find('.reservation-form-fields').toggleClass('none');
	}
	jQuery('.reservation-form-field-toggle').click(showFields);
});