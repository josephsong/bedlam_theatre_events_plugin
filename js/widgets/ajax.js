jQuery(document).ready(function(){
    try {
        var container = null;
        var callInProgress = false;
		jQuery.blockUI.defaults.css = {};
        function loadCalendar(e){
			if (jQuery(e.target).hasClass('.te-calendar-nav')) {
	            e.preventDefault();
	            if (!callInProgress) {
	                container = jQuery(jQuery(e.target).parents('.te-calendar:last').get(0));
					container.block({message: '<div class="loadingSpinner">Loading...</div>'});
	                callInProgress = true;
	                jQuery.get(
						e.target.rel,
						{}, 
						function(html){
		                    callInProgress = false;
							container.unblock();
							if (container) {
								container.html(html);
							}
		                }, 
						'html');
	            }
			}
        }
        
        jQuery('body').bind('click', loadCalendar);
        
    } 
    catch (err) {
        console.dir(err);
    }
});
