jQuery(document).ready(function() {
	try {
		
		
		function handleFocusBlur(e) {
			var trgt = jQuery(e.target);
			switch(e.type) {
			case 'focus':
				if (trgt.attr('title') == trgt.attr('value')) {
					trgt.attr('value', '');
					trgt.removeClass('default');
				} 
				break;
			default:
				if (trgt.attr('value') == '') {
					trgt.attr('value', trgt.attr('title'));
					trgt.addClass('default');
				} 
				break;
			}
		}
		
		var autoclears = jQuery('.autoclear');
		autoclears.bind('change focus blur', handleFocusBlur);
		
	} catch(err) {
		console.dir(err);
	}
});